from django.db import models
from django.core.mail import send_mail
import random

# Create your models here.

UNIDAD_ACADEMICA =(
    ('Agronomia','Agronomia'),
    ('Antropologia','Antropologia'),
    ('Artes','Artes'),
    ('Ciencia Politica','Ciencia Politica'),
    ('Ciencias Biologicas', 'Ciencias Biologicas'),
    ('Ciencias De La Salud','Ciencias De La Salud'),
    ('Ciencias De La Tierra','Ciencias De La Tierra'),
    ('Ciencias Sociales', 'Ciencias Sociales'),
    ('Contaduria Y Administracion', 'Contaduria Y Administracion'),
    ('Cultura','Cultura'),
    ('Derecho', 'Derecho'),
    ('Desarrollo Y Gestion Publica','Desarrollo Y Gestion Publica'),
    ('Docencia', 'Docencia'),
    ('Economia', 'Economia'),
    ('Enfermeria', 'Enfermeria'),
    ('Estudio De Las Humanidades','Estudio De Las Humanidades'),
    ('Estudios Nucleares','Estudios Nucleares'),
    ('Filosofia','Filosofia'),
    ('Fisica','Fisica'),
    ('Historia','Historia'),
    ('Ingenieria Electrica','Ingenieria Electrica'),
    ('Ingenieria 1','Ingenieria 1'),
    ('Letras','Letras'),
    ('Matematicas','Matematicas'),
    ('Medecina Humana','Medecina Humana'),
    ('Medecina Veterinaria','Medecina Veterinaria'),
    ('Musica','Musica'),
    ('Odontologia','Odontologia'),
    ('Preparatoria','Preparatoria'),
    ('Psicologia','Psicologia'),
    ('Secundaria','Secundaria'),
    ('Otra(o)','Otra(o)')
)

class Alumno (models.Model):
    curp = models.CharField(max_length=18)
    matricula = models.CharField(max_length=8,primary_key=True)
    nombre = models.CharField(max_length=40)
    apellido_paterno = models.CharField(max_length=15)
    apellido_materno = models.CharField(max_length=15)
    unidad_academica = models.CharField(max_length=40,choices=UNIDAD_ACADEMICA)
    correo_alterno = models.EmailField()
    def __str__(self):
        return f'{self.curp}{self.matricula}{self.nombre}{self.apellido_paterno}{self.apellido_materno}{self.unidad_academica}{self.correo_alterno}'

class Docente_Administrativo (models.Model):
    curp = models.CharField(max_length=18)
    matricula = models.CharField(max_length=6,primary_key=True)
    nombre = models.CharField(max_length=40)
    apellido_paterno = models.CharField(max_length=15)
    apellido_materno = models.CharField(max_length=15)
    unidad_academica = models.CharField(max_length=40,choices=UNIDAD_ACADEMICA)
    correo_alterno = models.EmailField()
    def __str__(self):
        return f'{self.curp}{self.matricula}{self.nombre}{self.apellido_paterno}{self.apellido_materno}{self.unidad_academica}{self.correo_alterno}'


class Correo_Alumno(models.Model):
    matricula = models.CharField(max_length=8)
    nombre = models.CharField(max_length=40)
    apellido_paterno = models.CharField(max_length=15)
    apellido_materno = models.CharField(max_length=15)
    unidad_academica = models.CharField(max_length=40, choices=UNIDAD_ACADEMICA)
    correo_alterno = models.EmailField()

    def __str__(self):
        texto = ' Tu correo es el siguiente :  ' + self.matricula+"@uaz.edu.mx"
        send_mail('Correo Institucional', texto, 'pruebasingsoftware@gmail.com', [self.correo_alterno])

        return f'{self.matricula}{self.nombre}{self.apellido_paterno}{self.apellido_materno}{self.unidad_academica}{self.correo_alterno}'

class Correo_Docente(models.Model):
    matricula = models.CharField(max_length=6)
    nombre = models.CharField(max_length=40)
    apellido_paterno = models.CharField(max_length=15)
    apellido_materno = models.CharField(max_length=15)
    unidad_academica = models.CharField(max_length=40, choices=UNIDAD_ACADEMICA)
    correo_alterno = models.EmailField()
    correo_institucional=  models.CharField(max_length=15)

    def __str__(self):
        texto = ' Tu correo es el siguiente :  ' + self.correo_institucional+"@uaz.edu.mx"
        send_mail('Correo Institucional', texto, 'pruebasingsoftware@gmail.com', [self.correo_alterno])

        return f'{self.matricula}{self.nombre}{self.apellido_paterno}{self.apellido_materno}{self.unidad_academica}{self.correo_alterno}'

class Recuperar_Correo(models.Model):
    matricula = models.CharField(max_length=8)
    nombre = models.CharField(max_length=40)
    correo_alterno = models.EmailField()
    correo_institucional = models.EmailField()

    def __str__(self):
        contra=random.sample("abcdefghijklmnopqrstuvwxyz",8)
        password="".join(contra)
        texto = 'Estimado '+self.nombre+' la contraseña del correo '+self.correo_institucional +' ha sido modificada, para su seguridad modifiquela pronto. Su nueva contraseña es: '+ password
        send_mail('Recuperacion De Correo Institucional',texto,'pruebasingsoftware@gmail.com',[self.correo_alterno])
        return f'{self.matricula}{self.correo_alterno}{self.correo_institucional}'
