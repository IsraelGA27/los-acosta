from django.apps import AppConfig


class CorreoinstitucionalbuazConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CorreoInstitucionalBUAZ'
