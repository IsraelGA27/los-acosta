from django.contrib import admin

from CorreoInstitucionalBUAZ.models import Alumno,Docente_Administrativo,Correo_Alumno,Correo_Docente,Recuperar_Correo
# Register your models here.

@admin.register(Alumno)
class ParticipanteAdmin (admin.ModelAdmin):
    list_display = ('curp','matricula','nombre','apellido_paterno','apellido_materno','unidad_academica','correo_alterno')
    list_filter = ('unidad_academica',)

@admin.register(Docente_Administrativo)
class Docente_AdministrativoAdmin (admin.ModelAdmin):
    list_display = ('curp','matricula','nombre','apellido_paterno','apellido_materno','unidad_academica','correo_alterno')
    list_filter = ('unidad_academica',)

@admin.register(Correo_Alumno)
class CorreoAdmin (admin.ModelAdmin):
    list_display = ('matricula', 'nombre', 'apellido_paterno', 'apellido_materno', 'unidad_academica', 'correo_alterno')

@admin.register(Correo_Docente)
class CorreoAdmin (admin.ModelAdmin):
    list_display = ('matricula', 'nombre', 'apellido_paterno', 'apellido_materno', 'unidad_academica', 'correo_alterno')

@admin.register(Recuperar_Correo)
class Recuperar_CorreoAdmin (admin.ModelAdmin):
    list_display = ('matricula','correo_alterno', 'correo_institucional')
